def get_input(filename):
    f = open(filename, "r")
    lines = f.readlines()[0]
    lines = lines.split(",")
    nums = len(lines)
    for i in range(0, nums):
        lines[i] = int(lines[i])
    return lines

def fuel_cost(num):
    ans = 0
    for i in range(1, num + 1):
        ans += i
    return ans

def average(nums):
    return (sum(nums)/len(nums))

def main():
    nums = get_input("input")
    minimum = min(nums)
    maximum = max(nums)
    cost = 0
    allcosts = {}
    for i in range(minimum, maximum + 1):
        for num in nums:
            cost += fuel_cost(abs(num - i))
        allcosts.update({i : cost})
        cost = 0
    anscost = min(allcosts.values())
    print(anscost)
    for costs in allcosts:
        if anscost == allcosts.get(costs):
            print(costs)

    


if __name__=="__main__":
    main()
