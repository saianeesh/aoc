class Fish():
    def __init__(self, timer):
        self.timer = timer


def get_input(filename):
    f = open(filename, "r")
    nums = f.readlines()[0].strip().split(",")
    for i in range(0, len(nums)):
        nums[i] = int(nums[i])
    return nums

def main():
    nums = get_input("test_input.txt")
    fish = []
    days = 256
    for num in nums:
        fish.append(Fish(num))

    for i in range(0, days):
        length = len(fish)
        for f in range(0, length):
            curr = fish[f]
            curr.timer -= 1
            if curr.timer < 0:
                curr.timer = 6
                fish.append(Fish(8))

    print(len(fish))
    


if __name__=="__main__":
    main()
