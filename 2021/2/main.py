def firstpart():
    f = open("input", "r")
    lines = f.readlines()
    f.close()
    horizontal = 0
    depth = 0
    aim = 0
    for line in lines:
        instructions = line.split()
        if instructions[0] == "forward":
            horizontal += int(instructions[1])
            depth += (aim * int(instructions[1]))
        elif instructions[0] == "down":
            aim += int(instructions[1])
            #depth += int(instructions[1])
        elif instructions[0] == "up":
            aim -= int(instructions[1])
            #depth -= int(instructions[1])
    print(horizontal * depth) 

if __name__=="__main__":
    firstpart()
