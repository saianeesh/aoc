length = 12
ones = [0] * length
zero = [0] * length
file = "input"
oxygen = 0
co2 = 0
def firstpart():
    f = open(file, "r")
    lines = f.readlines()
    f.close()
    gamma = ['0'] * length
    epsilon = ['0'] * length
    setup()
    for i in range(0, length):
        if ones[i] > zero[i]:
            gamma[i] = '1'
        elif zero[i] > ones[i]:
            epsilon[i] = '1'
    #print(int("".join(gamma), 2) * int("".join(epsilon), 2))

def setup(lines):
    global ones
    ones = [0] * length
    global zero
    zero = [0] * length
    for line in lines:
        for i in range(0, length):
            if line[i] == '1':
                ones[i] += 1
            elif line[i] == '0':
                zero[i] += 1

def secondpart():
    f = open(file, "r")
    lines = f.readlines()
    f.close()
    setup(lines)
    for i in range(0, length):
        if ones[i] >= zero[i]:
            l = 0
            while l != len(lines):
                line = lines[l]
                if line[i] == '0':
                    lines.pop(l)
                else:
                    l += 1
        else:
            l = 0
            while l != len(lines):
                line = lines[l]
                if line[i] == '1':
                    lines.pop(l)
                else:
                    l += 1
        if len(lines) == 1:
            break
        setup(lines)
    print("oxygen")
    global oxygen
    oxygen = int(lines[0], 2)
    print(lines)
    print(oxygen)

def secondparttwo():
    f = open(file, "r")
    lines = f.readlines()
    f.close()
    setup(lines)
    for i in range(0, length):
        if ones[i] >= zero[i]:
            l = 0
            while l != len(lines):
                line = lines[l]
                if line[i] == '1':
                    lines.pop(l)
                else:
                    l += 1
        else:
            l = 0
            while l != len(lines):
                line = lines[l]
                if line[i] == '0':
                    lines.pop(l)
                else:
                    l += 1
        if len(lines) == 1:
            break
        setup(lines)
    print("co2")
    print(lines)
    global co2
    global oxygen
    co2 = int(lines[0], 2)
    print(co2)
    print(co2 * oxygen)


if __name__=="__main__":
    secondpart()
    secondparttwo()
