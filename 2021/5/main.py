class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def __eq__(self, other):
        if self.x == other.x and self.y == other.y:
            return True
        return False

    def __hash__(self):
        return hash((self.x, self.y))

class Line:
    # startpoint and endpoint are Point() objects
    def __init__(self, startpoint, endpoint):
        self.start = startpoint
        self.end = endpoint

    def get_all_points(self):
        points = []
        if (self.start.x == self.end.x):
            # vertical line
            point = min(self.start.y, self.end.y)
            maximum = max(self.start.y, self.end.y)
            while point <= maximum:
                points.append(Point(self.start.x, point))
                point += 1
        else:
            endx = self.end.x
            endy = self.end.y
            startx = self.start.x
            starty = self.start.y
            m = calculate_slope(startx, starty, endx, endy)
            if m == None:
                return None
            c = starty - (m * startx)
            y = lambda x : m*x + c
            tmp = min(startx, endx)
            upper = max(startx, endx)
            while tmp <= upper:
                points.append(Point(tmp, y(tmp)))
                tmp += 1

        return points


def valid_line(line):
    if (line.start.x == line.end.x or line.start.y == line.end.y):
        return True
    else:
        return False

def calculate_slope(x1, y1, x2, y2):
    m = (y2 - y1)/(x2 - x1)
    if (m % 1) == 0:
        return int(m)
    else:
        return None

def get_input(filename):
    f = open(filename, "r")
    return f.readlines()

def main():
    lines = get_input("input")
    allpoints = {}
    for line in lines:
        endpoints = line.strip().split("->")
        for i in range(0, len(endpoints)):
            endpoints[i] = endpoints[i].strip()
        x1 = int(endpoints[0].split(",")[0])
        y1 = int(endpoints[0].split(",")[1])
        x2 = int(endpoints[1].split(",")[0])
        y2 = int(endpoints[1].split(",")[1])
        p1 = Point(x1, y1)
        p2 = Point(x2, y2)
        l1 = Line(p1, p2)
        points = l1.get_all_points()
        if points:
            for point in points:
                if point in allpoints:
                    allpoints[point] += 1
                else:
                    allpoints.update({point : 1})
    count = 0
    for key in allpoints:
        if allpoints.get(key) >= 2:
            count += 1
    print(count)

    


if __name__=="__main__":
    main()
