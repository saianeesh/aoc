#!/usr/bin/env -S python3


def firstpart():
    f = open("input", "r")
    count = 0
    prevnum = int(f.readline())
    for line in f:
        if (prevnum < int(line)):
            count += 1
        prevnum = int(line)
    f.close()
    return count


def secondpart():
    f = open("input", "r")
    lines = f.readlines()
    f.close()
    count = 0
    prevsum = int(lines[0]) + int(lines[1]) + int(lines[2])
    for i in range(1, len(lines)):
        if (i >= (len(lines) - 2)):
            break
        currsum = int(lines[i]) + int(lines[i+1]) + int(lines[i+2])
        if (prevsum < currsum):
            count += 1
        prevsum = currsum
    
    return count
    

if __name__ == "__main__":
    print(f"Part 1: {firstpart()}")
    print(f"Part 2: {secondpart()}")
