class Board:
    def __init__(self, *args):
        self.lines = []
        for line in args:
            self.lines.append(line.split())

    # matches a number on the board
    # if found, crosses, else nothing
    def cross(self, number):
        lineno = 0
        numlines = len(self.lines)
        while lineno < numlines:
            try:
                self.lines[lineno][self.lines[lineno].index(number)] = 'x'
                break
            except ValueError:
                pass
            lineno += 1

    def win(self):
        column = list(range(0, 5))
        for row in self.lines:
            count = 0
            for num in row:
                if num != 'x':
                    break
                if count == 4:
                    return True
                count += 1
        for colno in column:
            count = 0
            for row in self.lines:
                if row[colno] != 'x':
                    break
                if count == 4:
                    return True
                count += 1

    def calculate_win(self, lastnum):
        ans = 0
        for row in self.lines:
            for num in row:
                if num != 'x':
                    ans += int(num)
        ans *= int(lastnum)
        return ans

            
def get_input(filename):
    f = open(filename, "r")
    return f.readlines()

def main():
    lines = get_input("input")
    draw_from = lines[0].strip().split(',')
    boards = []
    boards_won = 0
    i = 2
    while i < len(lines):
        boards.append(Board(lines[i], lines[i + 1], lines[i + 2], lines[i + 3], lines[i + 4]))
        i += 6
    for num in draw_from:
        boardno = 0
        while boardno < len(boards):
            board = boards[boardno]
            board.cross(num)
            if (board.win()):
                boards_won += 1
                if len(boards) == 1:
                    print(board.calculate_win(num))
                    return
                boards.remove(board)
            else:
                boardno += 1




if __name__=="__main__":
    main()
