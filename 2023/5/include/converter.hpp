#ifndef _AOC_CONVERTER_
#define _AOC_CONVERTER_

#include <vector>
#include <map>
#include <iostream>
#include <iomanip>


using ull = unsigned long long;

enum Type {
	seed,
	soil,
	fertiliser,
	water,
	light,
	temperature,
	humidity,
	location
};

typedef struct {
	ull source;
	ull dest;
	ull len;
} Entry;

struct MapLink {
	Type from;
	Type to;

	friend bool operator==(const MapLink& lhs, const MapLink& rhs) {
		return ((lhs.from == rhs.from) && (lhs.to == rhs.to));
	}
};


const std::vector<std::string> titles =  {
	"seed-to-soil",
	"soil-to-fertilizer",
	"fertilizer-to-water",
	"water-to-light",
	"light-to-temperature",
	"temperature-to-humidity",
	"humidity-to-location"
};

const std::map<std::string, MapLink> type_lookup = {
	{ titles[0], { .from = Type::seed, .to = Type::soil } },
	{ titles[1], { .from = Type::soil, .to = Type::fertiliser } },
	{ titles[2], { .from = Type::fertiliser, .to = Type::water } },
	{ titles[3], { .from = Type::water, .to = Type::light } },
	{ titles[4], { .from = Type::light, .to = Type::temperature } },
	{ titles[5], { .from = Type::temperature, .to = Type::humidity } },
	{ titles[6], { .from = Type::humidity, .to = Type::location } }
};


class Mapping {
	public:
		MapLink link;
		std::vector<Entry> map;

		Mapping(Type from, Type to) {
			link = { .from = from, .to = to };
		}

		Mapping(MapLink l): link(l) {
			;
		}

		bool within_bounds(void);

		friend std::ostream& operator<< (std::ostream& os, const Mapping& m) {
			for (Entry e : m.map) {
				os << std::left << "Source: " << std::setw(20) << e.source << "Dest: " << std::setw(20) << e.dest << "Len: " << std::setw(20) << e.len << std::endl;
			}
			return (os);
		}

};


std::vector<std::string>
read_file(const std::string);

std::map<std::string, Mapping>
convert_mapping(const std::vector<std::string>&);


#endif
