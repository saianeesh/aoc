#include <iostream>
#include <vector>
#include <ctype.h>
#include <map>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <queue>
#include <limits>
#include <iomanip>

#include "converter.hpp"


std::vector<ull>
read_seeds(std::string s)
{
	std::vector<ull> seeds;
	ull temp;
	std::stringstream ss;

	ss << s.substr(s.find(':') + 1);
	while (ss >> temp) {
		seeds.push_back(temp);
	}

	return (seeds);
}


ull
seed_to_loc(const std::map<std::string, Mapping>& map, const ull seed)
{
	ull to = seed;
	int map_id = 0;

	for (std::string title : titles) {
		Mapping m = map.at(title);
		for (Entry e : m.map) {
			/* found correct mapping */
			if (to >= e.source && to <= e.source + e.len) {
				to = e.dest + (to - e.source);
				break;
			}
		}
	}

	return (to);
}

ull
part_one(const std::map<std::string, Mapping>& map, const std::vector<ull>& seeds)
{
	ull min = std::numeric_limits<ull>::max();

	for (ull seed : seeds) {
		ull tmp = seed_to_loc(map, seed);
		if (tmp < min) {
			min = tmp;
		}
	}

	return (min);
}

ull
part_two(const std::map<std::string, Mapping>& map, const std::vector<ull>& seeds)
{
	ull min = std::numeric_limits<ull>::max();

	for (size_t seed_pair = 0; seed_pair < seeds.size() - 1; seed_pair += 2) {
		std::cout << "Seed Pair: " << std::setw(5) << seed_pair << std::endl;
		ull seed_start = seeds.at(seed_pair);
		ull range = seeds.at(seed_pair + 1);
		for (ull seed = seed_start; seed < seed_start + range; seed++) {
			ull tmp = seed_to_loc(map, seed);
			if (tmp < min) {
				min = tmp;
			}
		}
	}

	return (min);
}

int
main(int argc, char *argv[])
{
	std::string filename;
	std::vector<std::string> input;
	std::map<std::string, Mapping> mapping;
	std::vector<ull> seeds;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	input = read_file(filename);
	seeds = read_seeds(input.at(0));
	input.erase(input.begin(), input.begin() + 2);

	mapping = convert_mapping(input);
	// print input
	/*
	for (std::string s : titles) {
		std::cout << "==============================================\n";
		std::cout << mapping.at(s) << std::endl;
	}
	*/

	//std::cout << "Part 1:\t" << part_one(mapping, seeds) << std::endl;
	std::cout << "Part 2:\t" << part_two(mapping, seeds) << std::endl;

	return (0);
}
