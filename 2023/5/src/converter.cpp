#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>

#include "converter.hpp"


std::map<std::string, Mapping>
convert_mapping(const std::vector<std::string>& input)
{
	std::map<std::string, Mapping> map;
	int map_index = 0;

	for (std::string title : titles) {
		Mapping temp = Mapping(type_lookup.at(title));
		
		/* find the correct mapping type */
		for (size_t i = map_index; i < input.size(); i++) {
			if (input.at(i).find(title) != std::string::npos) {
				map_index = i + 1;
				break;
			}
		}

		for (; ; map_index++) {
			try {
				if (input.at(map_index).empty()) {
					break;
				}
			} catch (std::out_of_range& e) {
				break;
			}
			std::stringstream ss(input.at(map_index));
			ull dest, source, len;

			ss >> dest;
			ss >> source;
			ss >> len;
			temp.map.push_back({
				.source = source,
				.dest = dest,
				.len = len 
			});
		}
		map.insert({title, temp});
	}

	return (map);
}




/* read file */
std::vector<std::string>
read_file(const std::string filename)
{
	std::string line;
	std::vector<std::string> input;
	int sum = 0;

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		exit(-1);
	}

	while (getline(file, line)) {
		input.push_back(line);
	}

	file.close();

	return (input);
}
