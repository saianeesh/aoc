#include <iostream>
#include <vector>
#include <ctype.h>
#include <fstream>
#include <map>
#include <sstream>
#include <stdexcept>
#include <algorithm>
#include <queue>


class Card {
	public:
		int id;
		int copies;
		int points;
		int matches;
		std::vector<int> win_nums;
		std::vector<int> nums;

		Card (int id = 0, int copies = 1) : id(id), copies(copies) {
			;
		}

		friend std::ostream& operator<< (std::ostream &os, const Card &c) {
			os << "ID: " << c.id << "\t POINTS: " << c.points << "\t COPIES: " << c.copies;
			return (os);
		}

		friend bool operator< (const Card &lhs, const Card &rhs) {
			return (lhs.id < rhs.id);
		}
};


void
create_cards(const std::vector<std::string> input, std::vector<Card> &cards)
{
	for (std::string card : input) {
		Card c;
		int num = 0;

		/* find card number */
		c.id = stoi(card.substr(card.find(' ') + 1, card.find(':') - card.find(' ') - 1));

		/* find winning numbers */
		{
			std::stringstream ss;
			ss << card.substr(card.find(':') + 2, card.find('|') - card.find(':') - 3);
			while (ss >> num) {
				c.win_nums.push_back(num);
			}
		}

		/* find your numbers */
		{
			std::stringstream ss;
			ss << card.substr(card.find('|') + 2);
			while (ss >> num) {
				c.nums.push_back(num);
			}
		}

		/* append to 'cards' parameter */
		cards.push_back(c);
	}
}


int
card_point(Card &c)
{
	int points = 0;
	int matches = 0;
	for (int win_num : c.win_nums) {
		if (std::find(c.nums.begin(), c.nums.end(), win_num) != c.nums.end()) {
			matches++;
			if (points > 0) {
				points *= 2;
			} else {
				points = 1;
			}
		}
	}
	c.points = points;
	c.matches = matches;

	return (points);
}


int
cards_point(std::vector<Card> &cards)
{
	int sum = 0;
	for (int i = 0; i < cards.size(); i++) {
		Card &c = cards.at(i);
		sum += card_point(c);
	}

	return (sum);
}

int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::vector<std::string> input;
	int sum = 0;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		return (-1);
	}
	while (getline(file, line)) {
		input.push_back(line);
	}
	file.close();

	/* part 1 */
	/* parse input into cards */
	std::vector<Card> cards;
	create_cards(input, cards);

	/* get points */
	sum = cards_point(cards);
	std::cout << "Total points: " << sum << std::endl;



	/* part 2 */
	/* go through each card (1 -> end) */
	for (int id = 1; id <= cards.size(); id++) {
		Card *c = &cards.at(id - 1);
		/* 
		 * for each card find how many copies of higher number cards it generates
		 * while considering how many copies there are of the current card 
		 */
		for (int i = c->id + 1; i <= c->id + c->matches; i++) {
			/* find card with id == i */
			for (int x = 0; x < cards.size(); x++) {
				try {
					if (cards.at(x).id == i) {
						cards.at(x).copies += c->copies;
						break;
					}
				} catch (std::out_of_range &e) {
					;
				}
			}
		}
	}


	int sum_cards = 0;
	for (Card c : cards) {
		sum_cards += c.copies;
	}
	std::cout << "TOTAL CARDS: " << sum_cards << std::endl;

	return (0);
}
