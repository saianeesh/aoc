#include <iostream>
#include <vector>
#include <ctype.h>
#include <fstream>
#include <map>
#include <sstream>
#include <stdexcept>

typedef struct {
	int row;
	int col;
} Loc;

typedef struct {
	Loc start;
	Loc end;
} NumBounds;

class SymbLoc {
	public:
		Loc loc;
		char symbol;

	friend bool operator< (const SymbLoc &lhs, const SymbLoc &rhs) {
		if (lhs.loc.row < rhs.loc.row) {
			return (true);
		} else if (lhs.loc.row == rhs.loc.row) {
			return (lhs.loc.col < rhs.loc.col);
		}

		return (false);
	}

	friend bool operator== (const SymbLoc &lhs, const SymbLoc &rhs) {
		if (lhs.loc.row == rhs.loc.row &&
				lhs.loc.col == rhs.loc.col && 
				lhs.symbol == rhs.symbol) {
			return (true);
		}
		return (false);
	}
};


/* returns all symbols adjacent to the given number in 'bound' */
std::vector<SymbLoc>
adj_symbols(const std::vector<std::string> input, const NumBounds bound)
{
	std::vector<SymbLoc> chars;
	NumBounds top_row = {
		{bound.start.row - 1, bound.start.col - 1},
		{bound.end.row - 1, bound.end.col + 1}
	};
	NumBounds bot_row = {
		{bound.start.row + 1, bound.start.col - 1},
		{bound.end.row + 1, bound.end.col + 1}
	};
	//std::cout << "top ROW: " << top_row.start.row << " COL: " << top_row.start.col << " END_COL: " << top_row.end.col << std::endl;
	//std::cout << "bot ROW: " << bot_row.start.row << " COL: " << bot_row.start.col << " END_COL: " << bot_row.end.col << std::endl;
	
	/* check left and right cells */
	try {
		char l_col = input.at(bound.start.row).at(bound.start.col - 1);
		if (l_col != '.') {
			chars.push_back({{bound.start.row, bound.start.col - 1}, l_col});
		}
	} catch (std::out_of_range &e) {
		;
	}

	try {
		char r_col = input.at(bound.start.row).at(bound.end.col + 1);
		if (r_col != '.') {
			chars.push_back({{bound.start.row, bound.end.col + 1}, r_col});
		}
	} catch (std::out_of_range &e) {
		;
	}

	/* collect top row symbols */
	for (int col = top_row.start.col; col <= top_row.end.col; col++) {
		try {
			char cell = input.at(top_row.start.row).at(col);
			if (cell != '.') {
				chars.push_back({{top_row.start.row, col}, cell});
			}
		} catch (std::out_of_range &e) {
			continue;
		}
	}

	/* collect bottom row symbols */
	for (int col = bot_row.start.col; col <= bot_row.end.col; col++) {
		try {
			char cell = input.at(bot_row.start.row).at(col);
			if (cell != '.') {
				chars.push_back({{bot_row.start.row, col}, cell});
			}
		} catch (std::out_of_range &e) {
			continue;
		}
	}

	return (chars);
}


/* is the number adjacent to a symbol? */
bool
part_number(const std::vector<std::string> input, const NumBounds bound, const char symb = 0)
{
	std::vector<SymbLoc> symbs = adj_symbols(input, bound);
	if (!symbs.empty()) {
		return (true);
	}

	return (false);
}

int
find_non_digit(const std::string row, const int pos = 0)
{
	if (pos < 0 || pos >= row.length()) {
		return (-1);
	}

	int i = pos;
	try {
		for (; isdigit(row.at(i)); i++);
	} catch (std::out_of_range &e) {
		;
	}

	return (i);
}

int
adj_sum(const std::vector<std::string> input)
{
	int sum = 0;
	for (int row = 0; row < input.size(); row++) {
		std::string row_str = input.at(row);
		for (int col = 0; col < row_str.length();) {
			if (isdigit(row_str.at(col))) {
				int end = (find_non_digit(row_str, col) == std::string::npos) ? row_str.length() - 1 : find_non_digit(row_str, col) - 1;
				NumBounds bound = {{row, col}, {row, end}};
				if (part_number(input, bound)) {
					sum += stoi(row_str.substr(col, end - col + 1));
				}
				col = end + 1;
				continue;
			}
			col++;
		}
	}

	return (sum);
}


int
gear_ratios(const std::vector<std::string> input)
{
	std::map<SymbLoc, std::vector<std::string>> adj_star;
	for (int row = 0; row < input.size(); row++) {
		std::string row_str = input.at(row);
		for (int col = 0; col < row_str.length();) {
			if (isdigit(row_str.at(col))) {
				int end = (find_non_digit(row_str, col) == std::string::npos) ? row_str.length() - 1 : find_non_digit(row_str, col) - 1;
				NumBounds bound = {{row, col}, {row, end}};
				std::vector<SymbLoc> adj_symbs = adj_symbols(input, bound);
				for (SymbLoc symb : adj_symbs) {
					if (symb.symbol == '*') {
						try {
							adj_star.at(symb).push_back(row_str.substr(col, end - col + 1));
						} catch (std::out_of_range &e) {
							std::vector<std::string> tmp; tmp.push_back(row_str.substr(col, end - col + 1));
							adj_star.insert(std::pair<SymbLoc, std::vector<std::string>>(symb, tmp));
						}
					}
				}
				col = end + 1;
				continue;
			}
			col++;
		}
	}

	int sum = 0;
	for (auto const &x : adj_star) {
		SymbLoc key = x.first;
		std::vector<std::string> value = x.second;
		if (value.size() == 2) {
			int prod = 1;
			for (std::string num : value) {
				prod *= stoi(num);
			}
			sum += prod;
		}
	}

	return (sum);
}


int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::vector<std::string> games_str;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		return (-1);
	}
	while (getline(file, line)) {
		games_str.push_back(line);
	}
	file.close();

	/* calculate sum */
	int sum = adj_sum(games_str);
	std::cout << sum << std::endl;

	sum = gear_ratios(games_str);
	std::cout << sum << std::endl;


	return (0);
}
