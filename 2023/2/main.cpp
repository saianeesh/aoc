#include <iostream>
#include <vector>
#include <ctype.h>
#include <fstream>
#include <map>
#include <sstream>
#include <stdexcept>

namespace colours {
	const std::string red = "red";
	const std::string green = "green";
	const std::string blue = "blue";
};

class Set {
	public:
		int red, green, blue;
		Set(int r = 0, int g = 0, int b = 0): red(r), green(g), blue(b) {
			;
		}
};


void
parse_input(const std::vector<std::string> input, std::map<size_t, std::vector<Set>> &out)
{

	for (size_t li = 0; li < input.size(); li++) {
		std::string line = input.at(li), set, line_sets;
		int start, end, game_id;
		std::stringstream set_stream;
		std::vector<Set> game;

		/* get game id */
		start = line.find(' ');
		end = line.find(':');
		if (start == std::string::npos || end == std::string::npos) {
			std::cerr << "Invalid input file!" << std::endl;
			exit(-1);
		}
		try {
			game_id = stoi(line.substr(start + 1, end - start - 1));
		} catch (const std::invalid_argument &e) {
			;
		} catch (const std::out_of_range &e) {
			std::cerr << "Invalid input file!" << std::endl;
		}

		/* go through each set */
		line_sets = line.substr(end + 2);
		set_stream << line_sets;
		while (!set_stream.eof()) {
			int r = 0, g = 0, b = 0;
			getline(set_stream, set, ';');
			std::stringstream item_stream(set);
			/* go through each item */
			while (!item_stream.eof()) {
				std::string item;
				getline(item_stream, item, ',');
				/* classify to red, green, and blue */
				std::string colour = item.substr(item.find(' ') + 1);
				if (colour.find(colours::red) != std::string::npos) {
					r = stoi(item.substr(0,item.rfind(' ')));
				} else if (colour.find(colours::blue) != std::string::npos) {
					b = stoi(item.substr(0,item.rfind(' ')));
				} else if (colour.find(colours::green) != std::string::npos) {
					g = stoi(item.substr(0,item.rfind(' ')));
				}
				item.clear();
			}
			game.push_back(Set(r, g, b));
			set.clear();
		}
		out.insert(std::pair<int, std::vector<Set>>(game_id, game));
	}
}


int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::vector<std::string> games_str;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		return (-1);
	}
	while (getline(file, line)) {
		games_str.push_back(line);
	}
	file.close();


	/* return the first and last digits of a string */
	std::map<size_t, std::vector<Set>> games;
	parse_input(games_str, games);
	/*
	for (std::pair<size_t, std::vector<Set>> game : games) {
		std::cout << "Game ID: " << std::get<0>(game) << std::endl;
		for (Set set : std::get<1>(game)) {
			std::cout << "Red: " << set.red;
			std::cout << " Green: " << set.green;
			std::cout << " Blue: " << set.blue;
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
	*/

	/* part 1 */
	const int max_red = 12, max_green = 13, max_blue = 14;
	int game_id_sum = 0;
	for (std::pair<size_t, std::vector<Set>> game : games) {
		bool tmp = false;
		for (Set set : std::get<1>(game)) {
			if (set.red > max_red) {
				tmp = true;
				break;
			}
			if (set.green > max_green) {
				tmp = true;
				break;
			}
			if (set.blue > max_blue) {
				tmp = true;
				break;
			}
		}
		game_id_sum += (tmp) ? 0 : std::get<0>(game);
	}

	std::cout << "Game ID sum: " << game_id_sum << std::endl;

	/* part 2 */
	int set_power_sum = 0;
	for (std::pair<size_t, std::vector<Set>> game : games) {
		int max_red = 1, max_blue = 1, max_green = 1;
		for (Set set : std::get<1>(game)) {
			if (set.red > max_red) {
				max_red = set.red;
			}
			if (set.green > max_green) {
				max_green = set.green;
			}
			if (set.blue > max_blue) {
				max_blue = set.blue;
			}
		}
		set_power_sum += (max_red * max_blue * max_green);
	}

	std::cout << "Sum of power: " << set_power_sum << std::endl;

	return (0);
}
