#!/usr/bin/python3

import sys
import math


def read_file(filename: str):
    with open(filename) as f:
        lines = f.readlines()
    time = [ int(lines[0].split()[i]) for i in range(1,len(lines[0].split())) ]
    distance = [ int(lines[1].split()[i]) for i in range(1,len(lines[1].split())) ]
    return [time, distance]

def part_one(time: list[int], distance: list[int]):
    max_dists_no = []
    for t_i in range(len(time)):
        dists_no = 0
        max_t = time[t_i]
        high_t  = (max_t + math.sqrt(math.pow(max_t, 2) - 4 * distance[t_i])) / 2
        low_t   = (max_t - math.sqrt(math.pow(max_t, 2) - 4 * distance[t_i])) / 2
        if (high_t % 1 != 0 and low_t % 1 != 0):
            # both floating points
            dists_no = math.floor(high_t) - math.ceil(low_t) + 1
        elif (high_t % 1 == 0 and low_t % 1 == 0):
            # both integer values
            dists_no = high_t - low_t - 1
        else:
            dists_no = math.floor(high_t - low_t)
            
        max_dists_no.append(dists_no)
    
    product = 1
    for dist in max_dists_no:
        product *= dist
    return (product)

def part_two(time: list[str], distance: list[str]):
    time_str = [ str(i) for i in time ]
    concat_time = ''
    for t in time_str:
        concat_time += t

    distance_str = [ str(i) for i in distance ]
    concat_distance = ''
    for t in distance_str:
        concat_distance += t

    return (part_one(time=[int(concat_time)], distance=[int(concat_distance)]))


def main(argv: list[str]):
    filename = "test_input.txt"
    if (len(argv) > 2):
        print("Wrong amount of arguments! Either 1 or 0 arguments expected!")
        sys.exit(1)
    if (len(argv) == 2):
        filename = argv[1]

    time, distance = read_file(filename)
    part_one_ans = part_one(time, distance)
    print(f"Part One Answer:\t{part_one_ans}")

    part_two_ans = part_two(time, distance)
    print(f"Part Two Answer:\t{part_two_ans}")



if __name__ == "__main__":
    main(sys.argv)
