#include <iostream>
#include <vector>
#include <ctype.h>
#include <fstream>

const std::vector<std::string> num_word = {
	"one",
	"two",
	"three",
	"four",
	"five",
	"six",
	"seven",
	"eight",
	"nine",
	"ten"
};

int
extract_num(const std::string line)
{
	std::vector<char> digits;
	for (char c : line) {
		if (isdigit(c)) {
			digits.push_back(c);
		}
	}
	
	return ((digits.front() - '0') * 10) + (digits.back() - '0');
}

int
extract_num(const std::string line, const std::vector<std::string> code)
{
	std::vector<int> digits;
	for (size_t i = 0; i < line.length(); i++) {
		char c = line.at(i);
		if (isdigit(c)) {
			digits.push_back(c - '0');
			continue;
		}
		for (size_t n = 0; n < code.size(); n++) {
			int f_i = line.find(code.at(n), i);
			if (f_i == i) {
				digits.push_back(n + 1);
			}
		}
	}

	return ((digits.front() * 10) + digits.back());
}

int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::vector<std::string> calibration;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		return (-1);
	}
	while (getline(file, line)) {
		calibration.push_back(line);
	}
	file.close();


	/* return the first and last digits of a string */
	int sum = 0;
	for (std::string line : calibration) {
		sum += extract_num(line, num_word);
	}
	std::cout << "Sum: " << sum << std::endl;

	return (0);
}
