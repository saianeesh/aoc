#include <iostream>
#include <vector>
#include <map>
#include <fstream>
#include <iomanip>

#include "grid.hpp"

std::vector<std::string>
read_file(const std::string);


int
main(int argc, char *argv[])
{
	std::string filename = "input.txt";
	std::vector<std::string> input;

	if (argc > 1) {
		filename = argv[1];
	}

	input = read_file(filename);
	Grid g;
	convert_input(input, g);
	for (; g.release_sand(); );

	for (const auto& [key, value] : g.grid) {
		for (const auto& [key1, value1] : value) {
			if (value1.type == Type::rest) {
				std::cout << std::setw(5) << key << std::setw(5) << key1 << std::setw(5) << 'o' << std::endl;
			}
		}
	}

	return (0);
}


/* read file */
std::vector<std::string>
read_file(const std::string filename)
{
	std::string line;
	std::vector<std::string> input;

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		exit(-1);
	}

	while (getline(file, line)) {
		input.push_back(line);
	}

	file.close();

	return (input);
}
