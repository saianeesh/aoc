#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <limits.h>

#include "grid.hpp"


int
Grid::bottom(void)
{
	int bottom = 0;
	for (const auto& [x, x_map] : grid) {
		for (const auto& [y, cell] : x_map) {
			if (y > bottom) {
				bottom = y;
			}
		}
	}

	return (bottom);
}

int
Grid::left(void)
{
	int left = INT_MAX;
	for (const auto& [x, x_map] : grid) {
		if (x < left) {
			left = x;
		}
	}

	return (left);
}


int
Grid::right(void)
{
	int right = 0;
	for (const auto& [x, x_map] : grid) {
		if (x > right) {
			right = x;
		}
	}

	return (right);
}


Cell
Grid::at(int x, int y)
{
	return (grid.at(x).at(y));
}

void
Grid::set(int x, int y, Type type)
{
	// x-coordinate
	try {
		std::map<int, Cell>& y = grid.at(x);
	} catch (std::out_of_range &e) {
		grid.insert({x, std::map<int, Cell>{}});
	}

	// y-coordinate
	try {
		grid.at(x).at(y).type = type;
	} catch (std::out_of_range& e) {
		grid.at(x).insert({y, {.type = type}});
	}

}

bool
Grid::release_sand(void)
{
	int bedrock = bottom();
	int sand[2] = {source[0], source[1]};
	for (;;) {
		if (sand[1] > bedrock) {
			return (false);
		}
		// chk down
		try {
			Cell& y = grid.at(sand[0]).at(sand[1] + 1);
		} catch (std::out_of_range& e) {
			sand[1]++;
			continue;
		}

		// chk down-left
		try {
			Cell& y = grid.at(sand[0] - 1).at(sand[1] + 1);
		} catch (std::out_of_range& e) {
			sand[1]++;
			sand[0]--;
			continue;
		}

		// chk down-right
		try {
			Cell& y = grid.at(sand[0] + 1).at(sand[1] + 1);
		} catch (std::out_of_range& e) {
			sand[1]++;
			sand[0]++;
			continue;
		}

		set(sand[0], sand[1], Type::rest);
		if (sand[0] == source[0] && sand[1] == source[1]) {
			return (false);
		}
		break;
	}

	return (true);
}


void
convert_input_line(const std::string& line, Grid& grid)
{
	std::vector<std::string> split_line;
	boost::split(
			split_line, 
			line, 
			boost::is_any_of(" -> "), 
			boost::token_compress_on
	);

	for (int coor_id = 0; coor_id < split_line.size() - 1; coor_id++) {
		std::string start = split_line.at(coor_id);
		std::string end = split_line.at(coor_id + 1);
		int start_x = stoi(start.substr(0, start.find(',') + 1));
		int start_y = stoi(start.substr(start.find(',') + 1));
		int end_x = stoi(end.substr(0, end.find(',') + 1));
		int end_y = stoi(end.substr(end.find(',') + 1));

		if (start_x == end_x) {
			for (int y = MIN(start_y, end_y); y <= MAX(start_y, end_y); y++) {
				grid.set(start_x, y, Type::rock);
			}
		} else if (start_y == end_y){
			for (int x = MIN(start_x, end_x); x <= MAX(start_x, end_x); x++) {
				grid.set(x, start_y, Type::rock);
			}
		} else {
			// error, wrong input
			std::cerr << "Wrong input formatting!" << std::endl;
			exit(1);
		}
	}
}

void
add_bedrock(const int bottom, Grid& grid)
{
	int buffer = 1000;
	int left = (INT_MIN + buffer > grid.left()) ? INT_MIN : grid.left() - buffer;
	int right = (INT_MAX - buffer < grid.right()) ? INT_MAX : grid.right() + buffer;

	for (int x = left; x <= right; x++) {
		grid.set(x, bottom, Type::rock);
	}
}

void
convert_input(const std::vector<std::string>& input, Grid& grid)
{
	for (std::string line : input) {
		convert_input_line(line, grid);
	}
	add_bedrock(grid.bottom() + 2, grid);
}


