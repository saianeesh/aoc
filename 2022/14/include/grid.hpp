#ifndef _AOC_GRID_
#define _AOC_GRID_

#include <map>


#define MAX(x, y) ((x >= y) ? x : y)
#define MIN(x, y) ((x >= y) ? y : x)

enum Type {
	rock	= '#',
	air		= '.',
	sand	= '+',
	rest	= 'o'
};

class Cell {
	public:
		Type type;
};

class Grid {
	public:
		const int source[2] = {500, 0};
		std::map<int, std::map<int, Cell>> grid;

		Cell at(int, int);
		void set(int, int, Type);
		bool release_sand(void);

		int bottom(void);
		int right(void);
		int left(void);
};


// x, y



void
convert_input(const std::vector<std::string>&, Grid&);

#endif
