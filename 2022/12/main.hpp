#ifndef _AOC_2022_12_
#define _AOC_2022_12_


/* a = 1, b = 2, c = 3, ... , z = 26 */
const std::string alphabet = "abcdefghijklmnopqrstuvwxyz";
enum Colour {
	white,
	black,
	grey 
};

typedef struct {
	int row;
	int col;
} Coor;


class Node {
	public:
		Coor loc;
		char letter;
		int elev;
		int dist;
		bool explored;
		enum Colour colour;
		/* implies there is only from self->other nodes 
		 * not other->self
		 */
		std::vector<Node *> conn;

	Node(char letter, int elev, Coor loc = {-1, -1}, int dist = 0, bool explored = false) :
	letter(letter), elev(elev), loc(loc), dist(dist), explored(explored)
	{
		;
	}

	friend bool operator==(const Node &lhs, const Node &rhs) {
		if (lhs.loc.row == rhs.loc.row && lhs.loc.col == rhs.loc.col) {
			return true;
		}
		return false;
	}

	friend std::ostream &operator<<(std::ostream &os, const Node &obj) {
		os << "(" << "[" << obj.loc.row << ", " << obj.loc.col << "], " << obj.letter << ", " << obj.conn.size() << ")";
		return os;
	}
};


#endif
