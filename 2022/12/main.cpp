#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>
#include <cmath>

#include "main.hpp"

/*
 * BFS using adjacency list
 *
 * @param graph		the adjacency list
 * @param start		the starting cell (row, col). diff is ignored
 * @param dest		the destination cell (row, col). diff is ignored
 */
int bfs(const std::vector<Node *> &graph, Node *start, const Node *dest, const size_t ncols)
{
	size_t start_index = start->loc.row * ncols + start->loc.col;
	size_t dest_index = dest->loc.row * ncols + dest->loc.col;
	std::vector<Node *> Q;

	// initialise
	Q.push_back(start);

	//int i = 0;
	for (Node *n = Q.at(0); !Q.empty(); n = Q.at(0)) {
		// if destination node then exit
		if (*n == *dest) {
			break;
		}
		//std::cout << i++ << std::endl;
		//std::vector<Node *> adj_list = graph.at(n->loc.row * ncols + n->loc.col)->conn;
		for (size_t i = 0; i < n->conn.size(); i++) {
			Node *adj = n->conn.at(i);
			if (!adj->explored && (adj->elev - n->elev) <= 1) {
				if (adj->dist <= 0 || adj->dist > n->dist + 1) {
					adj->dist = n->dist + 1;
				}
				if (!adj->explored) {
					Q.push_back(adj);
				}
				adj->explored = true;
			}
		}
		if (Q.erase(Q.begin()) == Q.end()) {
			break;
		}
	}

	return (dest->dist);
}


/*
 * fill in the `all_nodes` parameter
 */
void
str_to_graph(const std::vector<std::string> &text, std::vector<Node *> &all_nodes)
{
	size_t nrows = text.size();
	size_t ncols = text.front().length();

	/* create nodes for each cell */
	for (size_t i = 0; i < nrows * ncols; i++) {
		size_t row = i / ncols;
		size_t col = i % ncols;
		char curr_char = text.at(row).at(col);
		int elevation = alphabet.find(curr_char) + 1;
		if (curr_char == 'S') {
			elevation = 1;
		} else if (curr_char == 'E') {
			elevation = 26;
		}
		all_nodes.push_back(new Node(curr_char, elevation, {static_cast<int>(row), static_cast<int>(col)}));
	}

	/* assign the neighbour nodes for each cell */
	for (size_t i = 0; i < nrows * ncols; i++) {
		size_t row = i / ncols;
		size_t col = i % ncols;
		char curr_char = text.at(row).at(col);
		int curr_elev = alphabet.find(curr_char) + 1;
		try {
			/* UP */
			int diff = (alphabet.find(text.at(row - 1).at(col)) + 1) - curr_elev;
			if (diff <= 1) {
				// adds from node i -> the one above (if it exists)
				all_nodes.at(i)->conn.push_back(all_nodes.at((row - 1) * ncols + col));
			}
		} catch (std::out_of_range &e) {
			//std::cerr << "Out of range: " << i << std::endl;
		}
		try {
			/* DOWN */
			int diff = (alphabet.find(text.at(row + 1).at(col)) + 1) - curr_elev;
			if (diff <= 1) {
				all_nodes.at(i)->conn.push_back(all_nodes.at((row + 1) * ncols + col));
			}
		} catch (std::out_of_range &e) {
			//std::cerr << "Out of range: " << i << std::endl;
		}
		try {
			/* LEFT */
			int diff = (alphabet.find(text.at(row).at(col - 1)) + 1) - curr_elev;
			if (diff <= 1) {
				all_nodes.at(i)->conn.push_back(all_nodes.at(row * ncols + (col - 1)));
			}
		} catch (std::out_of_range &e) {
			//std::cerr << "Out of range: " << i << std::endl;
		}
		try {
			/* RIGHT */
			int diff = (alphabet.find(text.at(row).at(col + 1)) + 1) - curr_elev;
			if (diff <= 1) {
				all_nodes.at(i)->conn.push_back(all_nodes.at(row * ncols + (col + 1)));
			}
		} catch (std::out_of_range &e) {
			//std::cerr << "Out of range: " << i << std::endl;
		}
	}

}



int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::vector<std::string> elevation;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	/* open and read file lines into elevation */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		return (-1);
	}
	while (getline(file, line)) {
		elevation.push_back(line);
	}
	file.close();

	/* convert text to graph */
	std::vector<Node *> all_nodes;
	str_to_graph(elevation, all_nodes);
	// print each cell character
	/*
	for (Node *p : all_nodes) {
		std::cout << p->letter << std::endl;
	}
	*/

	// print each cell neighbours
	/*
	for (size_t i = 0; i < all_nodes.size(); i++) {
		Node *entry = all_nodes.at(i);
		std::cout << "=================CELL " << i << " ==================" << std::endl;
		for (Node *p : entry->conn) {
			std::cout << "Row: " << p->loc.row << "\tCol: " << p->loc.col << "\tElevation: " << p->elev << "\tLetter: " << p->letter << std::endl;
		}
		std::cout << "================= CELL END ==================" << std::endl;
	}
	*/
	Node *start, *end;
	for (Node *n : all_nodes) {
		if (n->letter == 'S') {
			start = n;
		} else if (n->letter == 'E') {
			end = n;
		}
	}

	std::vector<int> shortest_paths;

	/* BFS to find smallest path to destination */
	shortest_paths.push_back(bfs(all_nodes, start, end, elevation.front().length()));
	std::cout << "Shortest path from the given starting point: " << shortest_paths.front() << std::endl;

	/* shortest path from all 'a' paths */
	for (size_t i = 0; i < all_nodes.size(); i++) {
		start = all_nodes.at(i);
		if (start->letter == 'a') {
			for (size_t x = 0; x < all_nodes.size(); x++) {
				Node *x_n = all_nodes.at(x);
				x_n->explored = false;
				x_n->dist = 0;
			}
			try {
				int shortest_path = bfs(all_nodes, start, end, elevation.front().length());
				if (shortest_path == 0) {
					;
				} else {
					shortest_paths.push_back(shortest_path);
				}
			} catch (std::out_of_range &e) {
				std::cerr << "Out of Range (n->conn): " << i << std::endl;
				return (-1);
			}
		}
	}
	std::vector<int>::iterator min_steps = std::min_element(shortest_paths.begin(), shortest_paths.end());
	/*
	for (int a : shortest_paths) {
		std::cout << a << std::endl;
	}
	*/
	std::cout << "Shortest path from the all starting points: " << *min_steps << std::endl;

	return (0);
}
