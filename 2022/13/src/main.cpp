#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>
#include <cmath>
#include <array>

#include "compare.hpp"

int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::vector<std::string> packets;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	/* open and read file lines into packets */
	std::fstream file(filename);
	if (!file.is_open()) {
		std::cerr << "Can't open file." << std::endl;
		file.close();
		return (-1);
	}
	while (getline(file, line)) {
		packets.push_back(line);
	}
	file.close();

	compare(packets);


	return (0);
}
