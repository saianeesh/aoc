#include <vector>
#include <iostream>
#include <array>
#include <stack>

#MAX(a, b) (a > b ? a : b)

class Packet {
	public:
		std::string data;


};

void
compare_pair(const std::array<std::string, 2>& pair)
{
	std::string p1 = pair[0], p2 = pair[1];
	std::stack<size_t> open_brackets;
	for (size_t i = 0; i < MAX(p1.length(), p2.length()); i++) {
		char cp1, cp2;
		try {
			cp1 = p1[i];
		} catch (std::out_of_range& e) {
			;
		}

		try {
			cp2 = p2[i];
		} catch (std::out_of_range& e) {
			;
		}
	}

}


void
compare(const std::vector<std::string>& input)
{
	for (int i = 0; i < input.size(); i++) {
		std::string p = input.at(i);
		/* next pair */
		if (p == "\n") {
			continue;
		}

		/* a packet */
		std::array<std::string, 2> pair = { p, input.at(++i)};
		compare_pair(pair);
	}
}
