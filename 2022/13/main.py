#!/usr/bin/env python

import sys

class Pair:
    p1 = []
    p2 = []

    def __init__(self, p1, p2):
        self.p1 = p1
        self.p2 = p2

def read_packet(ps: str):
    ans = []
    for i in range(len(ps)):
        if ps[i] == "[":
            try:
              read_packet(ps[i:ps.rindex("]")])
            except e:
                print("Wrong formatting")
                sys.exit(1)
        elif ps[i] == ",":
            pass



def get_input(filename):
    f = open(filename, "r")
    lines = f.readlines()
    pair = []
    packets = []
    for line in lines:
        if line != '\n':
            pair.append(read_packet(line[1:-1]))
        else:
            packets.append(Pair(pair[0], pair[1]))







def main(argv):
    lines = get_input("input.txt");


if __name__ == "__main__":
    main(sys.argv)
