#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <algorithm>

namespace aoc_input {
	const char colon = ':';
	const char space = ' ';
	const char comma = ',';
	const char equal = '=';
};

namespace operation {
	const char add = '+';
	const char sub = '-';
	const char mul = '*';
	const char div = '/';
};

/*
 * Monkey class
 *		- constructor : parsing the input + finds operation string + finds both tests
 *		- items : list
 *		- operation : function(std::string) (string of form "a * b")
 *		- test: function(int number) ()
 */
class Monkey {
	public:
		size_t id;
		std::vector<unsigned long long> items;
		std::map<std::string, int> operation;
		int divisible;
		int if_true, if_false;
		long long num_inspect;

		Monkey(std::vector<std::string>);
		void perform_turn(std::vector<Monkey> &);

	private:
		/* returns which monkey to pass to. -1 on error */
		bool perform_test(std::vector<Monkey> &);

		friend std::ostream& operator<<(std::ostream &os, const Monkey &m) {
			os << "[Monkey] " << m.id << "\t";
			os << "[Items] ";
			for (unsigned long long i : m.items) {
				os << i << ", ";
			}
			//os << "\t";
			//os << "[Operation] " << m.operation.at("operand") << "," << (char) m.operation.at("operation") << "," << m.operation.at("op_right") << "\t";
			//os << "[Divisible] " << m.divisible << "," << m.if_true << "," << m.if_false << "\t";
			return (os);
		}
};

/*
 * input is an array of lines
 */
Monkey::Monkey(std::vector<std::string> in) : num_inspect(0)
{
	std::string id_line, items_line, items_list, op_line;
	size_t index, op_index;

	/* find id */
	id_line = in[0];
	index = id_line.rfind(aoc_input::space) + 1;
	id = std::stoi(id_line.substr(index, id_line.size() - index - 1));
	
	/* read items */
	items_line = in[1] + ",";
	index = items_line.rfind(aoc_input::colon) + 2;
	items_list = items_line.substr(index);
	for (size_t i = 0; i < items_list.size(); i++) {
		if (items_list[i] == aoc_input::comma || i == items_list.size() - 1) {
			items.push_back(stoi(items_list.substr(0, i)));
			items_list = items_list.substr(i + 1);
			i = 0;
		}
	}

	/* operation */
	// operands are either numbers or 'old'
	op_line = in[2];
	index = op_line.rfind(aoc_input::equal) + 1;
	op_line = op_line.substr(index);
	if ((op_index = op_line.find(operation::add)) != std::string::npos) {
		;
	} else if ((op_index = op_line.find(operation::sub)) != std::string::npos) {
		;
	} else if ((op_index = op_line.find(operation::mul)) != std::string::npos) {
		;
	} else {
		// div
		;
	}
	if (op_line.substr(0, op_index - 1).find("old") != std::string::npos && op_line.substr(op_index + 1).find("old") == std::string::npos) {
		// ONLY left operand is 'old' -> right is integer
		operation = {
			{"operand", stoi(op_line.substr(op_index + 1))},
			{"operation", op_line[op_index]},
			{"op_right", 0}
		};
	} else if (op_line.substr(0, op_index - 1).find("old") != std::string::npos && op_line.substr(op_index + 1).find("old") == std::string::npos) {
		// ONLY right operand is "old" -> left is integer
		operation = {
			{"operand", stoi(op_line.substr(0, op_index - 1))},
			{"operation", op_line[op_index]},
			{"op_right", 1}
		};
	} else {
		// BOTH are 'old'
		operation = {
			{"operand", -1},
			{"operation", op_line[op_index]},
			{"op_right", -1},
		};
	}

	/* test */
	// lines 3, 4, 5
	op_line = in[3];
	divisible = stoi(op_line.substr(op_line.rfind(aoc_input::space) + 1));
	op_line = in[4];
	if_true = stoi(op_line.substr(op_line.rfind(aoc_input::space) + 1));
	op_line = in[5];
	if_false = stoi(op_line.substr(op_line.rfind(aoc_input::space) + 1));
}


void
Monkey::perform_turn(std::vector<Monkey> &ms)
{
	size_t items_size = items.size();
	for (int i = 0; i < items_size; i++) {
		perform_test(ms);
	}
}

bool
Monkey::perform_test(std::vector<Monkey> &ms)
{
	/* perform operation */
	unsigned long long item = items.front();
	unsigned long long ans = 0;
	unsigned long long a = \
		(operation.at("operand") < 0) ? item : operation.at("operand");
	char op = operation.at("operation");

	items.erase(items.begin());

	switch (op) {
		case operation::add:
			ans = item + a;
			break;
		/*
		case operation::sub:
			if (operation.at("op_right")) {
				// operation on right side, non-old operand on left-side
				ans = a - item;
			} else {
				ans = item - a;
			}
			break;
		case operation::div:
			if (operation.at("op_right")) {
				// operation on right side, non-old operand on left-side
				ans = a / item;
			} else {
				ans = item / a;
			}
			break;
		*/
		case operation::mul:
			ans = item * a;
			break;
		default:
			std::cerr << "Undefined operator: " << op << std::endl;
			exit(-1);
	}

	/* which monkey to pass to */
	ans /= 3;
	int p_div = 1;
	for (Monkey m : ms) {
		p_div *= m.divisible;
	}
	Monkey *pass_to;
	if ((ans % divisible) == 0) {
		pass_to = &(ms.at(if_true));
	} else {
		pass_to = &(ms.at(if_false));
	}
	pass_to->items.push_back(ans % p_div);

	num_inspect++;
	return true;
}


int
open_file(std::string filename, std::fstream *input)
{
	(*input).open(filename, std::ios::in);

	if (!(*input).is_open()) {
		return (-1);
	}

	return (0);
}

bool
comp_monkeys(const Monkey &a, const Monkey &b)
{
	return (a.id < b.id);
}

int
main(int argc, char *argv[])
{
	std::string filename, line;
	std::fstream input;
	std::vector<std::string> monkey_entry;
	std::vector<Monkey> monkeys;

	if (argc <= 1) {
		filename = "input.txt";
	} else {
		filename = argv[1];
	}

	if (open_file(filename, &input)) {
		std::cerr << "Can't open file." << std::endl;
		input.close();
		return (-1);
	}

	/* parse input and fill in 'monkeys' vector */
	while (getline(input, line)) {
		if (line.empty()) {
			// create Monkey
			monkeys.push_back(Monkey(monkey_entry));
			// reset monkey_entry
			monkey_entry.clear();
		} else {
			// not equal to ""
			monkey_entry.push_back(line);
		}
	}
	input.close();

	/* sort monkeys in ascending order */
	std::sort(monkeys.begin(), monkeys.end(), comp_monkeys);

	/* rounds */
	for (int round = 1; round <= 20; round++) {
		for (int i = 0 ; i < monkeys.size(); i++) {
			monkeys.at(i).perform_turn(monkeys);
		}
		/*
		std::cout << "==========ROUND " << round << "===========" << std::endl;
		for (Monkey m : monkeys) {
			std::cout << m << std::endl;
		}
		*/
	}

	for (Monkey m : monkeys) {
		std::cout << m.num_inspect << "\t | ";
		std::cout << m << std::endl;
	}



	unsigned long long max = 0;
	for (Monkey m : monkeys) {
		if (m.num_inspect > max) {
			max = m.num_inspect;
		}
	}

	unsigned long long max2 = 0;
	for (Monkey m : monkeys) {
		if (m.num_inspect < max && m.num_inspect > max2) {
			max2 = m.num_inspect;
		}
	}

	std::cout << "Monkey Business: " << max2 * max << std::endl;
	
	return (0);
}
